package core;

public class GitCommitObject extends GitBlobObject {

    public GitCommitObject(String path, String hash) {
        super(path, hash);
    }

    public String getTreeHash() throws IllegalAccessException {
        String content = this.getContent();
        return content.split("\n")[0].split(" ")[1];
    }

    public String getParentHash() throws IllegalAccessException {
        String content = this.getContent();
        return content.split("\n")[1].split(" ")[1];
    }

    public String getAuthor() throws IllegalAccessException {
        String content = this.getContent();
        String author = content.split("\n")[2];
        return author.substring(author.indexOf(" ")+1, author.indexOf(">")+1);
    }
}
