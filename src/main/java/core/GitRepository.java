package core;

import java.io.BufferedReader;
import java.nio.file.Files;
import java.nio.file.Paths;

public class GitRepository {

    private String _path;

    public GitRepository(String path) {
        this._path = path;
    }

    public String getHeadRef() {
        try (BufferedReader br = Files.newBufferedReader(Paths.get(this._path + "/HEAD"))) {
            String content = br.readLine();
            return content.replace("ref: ", "");
        }
        catch (Exception e) {}
        return null;
    }

    public String getRefHash(String ref) {
        try (BufferedReader br = Files.newBufferedReader(Paths.get(this._path + "/" + ref))) {
            return br.readLine();
        }
        catch (Exception e) {}
        return null;
    }
}
