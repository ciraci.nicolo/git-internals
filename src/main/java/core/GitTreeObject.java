package core;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.zip.InflaterInputStream;

public class GitTreeObject extends GitBlobObject {

    public class GitTreeTouple {
        public String filename;
        public String hash;

        public GitTreeTouple(String filename, String hash) {
            this.filename = filename;
            this.hash = hash;
        }

    }

    public GitTreeObject(String path, String hash) {
        super(path, hash);
    }

    // Git tree file encoding is slightly different from the other, it features TWO encoding (WHY)
    public ArrayList<GitTreeTouple> getTreeObjectContent() throws IllegalAccessException {

        try {
            FileInputStream fileInputStream = new FileInputStream(this.objectStorePath());
            InflaterInputStream inStream = new InflaterInputStream(fileInputStream);
            ArrayList<GitTreeTouple> files = new ArrayList<>();

            int i;
            while(inStream.read() != 0){
                //First line, gonna ignore
            }

            while(inStream.read() != -1) {
                while(inStream.read() != 0x20){ /* Permission */ }

                String filename = "";
                while((i = inStream.read()) != 0){
                    filename += (char) i;
                }

                String hash = "";
                for(int count = 0; count < 20 ; count++){
                    i = inStream.read();
                    hash += String.format("%02x", i);
                }

                files.add(new GitTreeTouple(filename, hash));
            }

            return files;
        } catch (Exception e) {
            throw new IllegalAccessException("Unable to get object content");
        }
    }

    public String[] getEntryPaths() throws IllegalAccessException {
        ArrayList<GitTreeTouple> files = this.getTreeObjectContent();
        List<String> list = files.stream().map(f -> f.filename).collect(Collectors.toList());

        String lf[] = new String[list.size()];
        return list.toArray(lf);
    }

    public GitBlobObject getEntry(String name) throws IllegalAccessException {
        ArrayList<GitTreeTouple> files = this.getTreeObjectContent();
        Optional<GitTreeTouple> first = files.stream().filter(f -> f.filename.equals(name)).findFirst();

        if (first.isPresent()) {
            String hash = first.get().hash;
            GitBlobObject fakeObject = new GitBlobObject(this._path, hash);

            switch (fakeObject.getType()) {
                case "blob": return fakeObject;
                case "commit": return new GitCommitObject(this._path, hash);
                case "tree": return new GitTreeObject(this._path, hash);
            }
        }

        return null;
    }
}
