package core;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Scanner;
import java.util.zip.InflaterInputStream;

public class GitBlobObject {
    protected String _path;
    protected String _hash;

    public GitBlobObject(String path, String hash) {
        this._path = path;
        this._hash = hash;
    }

    protected String objectStorePath() {

        return this._path + "/objects/" + this._hash.substring(0, 2) + "/" + this._hash.substring(2);
    }

    protected String getObjectContent() throws IllegalAccessException {
        try {
            InputStream in = new InflaterInputStream(new FileInputStream(this.objectStorePath()));
            Scanner scanner = new Scanner(in).useDelimiter("\\A");
            String content = scanner.next();
            return content;

        } catch (Exception e) {
            throw new IllegalAccessException("Unable to get object content");
        }
    }

    public String getType() throws IllegalAccessException {
        String content = this.getObjectContent();
        String typeAndSize = content.substring(0, content.indexOf(0));
        return typeAndSize.split(" ")[0];
    }

    public String getContent() throws IllegalAccessException {
        String content = this.getObjectContent();
        String blobData = content.substring(content.indexOf(0)+1);
        return blobData;
    }

    public String getHash() {
        return this._hash;
    }
}
